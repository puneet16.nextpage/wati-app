import React, { useState, ChangeEvent } from "react";
import { postData } from "./api/apiService";

interface FormData {
  input1: string;
  input2: string;
}

function App() {
  const [formData, setFormData] = useState<FormData>({
    input1: "",
    input2: "",
  });

  const [records] = useState<[]>([]);
  const [input1Error, setInput1Error] = useState<string>("");
  const [input2Error, setInput2Error] = useState<string>("");
  const [output, setOutput] = useState<string | null>(null);
  const [loading, setLoading] = useState<boolean>(false);
  const [error, setError] = useState(null);

  const handleInputChange = (event: ChangeEvent<HTMLInputElement>) => {
    const { name, value } = event.target;
    const pattern = /^-?\d*$/; // Pattern to check for integers (with optional negative sign)
    if (pattern.test(value)) {
      setFormData((prevData) => ({
        ...prevData,
        [name]: value,
      }));
    }
  };

  const handleLogValues = async () => {
    const int1 = parseInt(formData.input1);
    const int2 = parseInt(formData.input2);

    if (isNaN(int1) || isNaN(int2)) {
      setInput1Error(isNaN(int1) ? "Please enter a valid integer." : "");
      setInput2Error(isNaN(int2) ? "Please enter a valid integer." : "");
      setOutput(null);
      return;
    }

    try {
      setInput1Error("");
      setInput2Error("");
      setLoading(true);
      const response: any = await postData("/api/Integer/Add", {
        value1: int1,
        value2: int2,
      });
      console.log("Response:", response);
      if (response && response.data) {
        setOutput(response.data);
      }
    } catch (error: any) {
      setError(error.message);
    } finally {
      setLoading(false);
    }
  };

  return (
    <div className='container mt-5'>
      <div className='row'>
        <div className='col-md-6 offset-md-3'>
          <div className='mb-3'>
            <label htmlFor='input1' className='form-label'>
              Enter Integer 1:
            </label>
            <input
              type='text'
              className={`form-control ${input1Error && "is-invalid"}`}
              id='input1'
              name='input1'
              value={formData.input1}
              onChange={handleInputChange}
              placeholder='Enter integer 1'
            />
            {input1Error && (
              <div className='invalid-feedback'>{input1Error}</div>
            )}
          </div>
          <div className='mb-3'>
            <label htmlFor='input2' className='form-label'>
              Enter Integer 2:
            </label>
            <input
              type='text'
              className={`form-control ${input2Error && "is-invalid"}`}
              id='input2'
              name='input2'
              value={formData.input2}
              onChange={handleInputChange}
              placeholder='Enter integer 2'
            />
            {input2Error && (
              <div className='invalid-feedback'>{input2Error}</div>
            )}
          </div>
          <button
            type='button'
            className='btn btn-primary'
            onClick={handleLogValues}
            disabled={loading}
          >
            Save
          </button>
          {error && <div className='text-danger mt-2'>Error: {error}</div>}
          {loading && (
            <div className='mt-3'>
              <div className='spinner-border text-secondary' role='status'>
                <span className='visually-hidden'>Loading...</span>
              </div>
            </div>
          )}
          {output && !loading && <div className='mt-3'>Response: {output}</div>}
          <div className='mt-5'>
            <h5>Results</h5>
            <table className='table mt-3'>
              <thead>
                <tr>
                  <th scope='col'>Input 1</th>
                  <th scope='col'>Input 2</th>
                  <th scope='col'>Result</th>
                </tr>
              </thead>
              <tbody>
                {records.length ? (
                  <tr>
                    <td>{"1"}</td>
                    <td>{"2"}</td>
                    <td>{"3"}</td>
                  </tr>
                ) : (
                  <tr>
                    <td colSpan={3} className='text-center'>
                      No Data
                    </td>
                  </tr>
                )}
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  );
}

export default App;
