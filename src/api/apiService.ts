import axios, { AxiosInstance, AxiosResponse, AxiosError } from "axios";

const BASE_URL = "https://8468-103-177-174-108.ngrok-free.app";

const apiService: AxiosInstance = axios.create({
  baseURL: BASE_URL,
  timeout: 5000, // Set a timeout for requests (in milliseconds)
});

// Function to make a POST request
export const postData = async <T>(endpoint: string, data: any): Promise<T> => {
  try {
    const response: AxiosResponse<T> = await apiService.post(endpoint, data);
    console.log("repsonse", response);
    return response.data;
  } catch (error) {
    throw (error as AxiosError).response?.data || error;
  }
};

// Function to make a GET request
export const getData = async <T>(
  endpoint: string,
  params: any = {}
): Promise<T> => {
  try {
    const response: AxiosResponse<T> = await apiService.get(endpoint, {
      params,
    });
    return response.data;
  } catch (error) {
    throw (error as AxiosError).response?.data || error;
  }
};

export default apiService;
